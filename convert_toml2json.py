import tomli
import json
import logging
from pathlib import Path
import sys
import glob
import os


logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)  # INFO - DEBUG



try:
    logger.debug(sys.argv[1])
    logger.debug(sys.argv[2])
    if os.path.isdir(sys.argv[1]) and os.path.isdir(sys.argv[2]):
        pass
    else:
        exit("No source or destination directory provided")
except Exception as e:
    raise e
    exit("Unable to read sys arguments")

# Get list of files in the source directory
toml_file_list = glob.glob(f"{sys.argv[1]}/*.toml")
logger.debug(f"toml file list: {toml_file_list}")


def load_toml(toml_file):
    """Load toml data from file"""
    with open(toml_file, "rb") as f:
        logger.debug(f"loading toml file: {toml_file}")
        return tomli.load(f)


def convert_toml_to_json(toml_file_list):
    # Set output directory under 'docs/converted_json_files'
    output_dir = f'{sys.argv[2]}/'
    os.makedirs(output_dir, exist_ok=True)
    
    combined_json_path = os.path.join(output_dir, 'combined_cards.json')
    combined_data = []  # List to hold all the extracted data

    for i in toml_file_list:
        try:
            toml_dict = load_toml(i)
            logger.info(f"loading toml data for {i}")

            file_name_stem = Path(i).stem
            # Add file name
            toml_dict["file_name"] = file_name_stem

            # Append parsed TOML data to the combined list

            combined_data.append(toml_dict)
            logger.info(f"Successfully extracted data from {i}")

        except Exception as e:
            logger.info(f"Failed to extract data from {i}: {e}")

    # Save all extracted data to one JSON file
    with open(combined_json_path, 'w') as json_file:
        json.dump(combined_data, json_file, indent=4)
    
    logger.info(f"All data has been saved to {combined_json_path}")

# Convert the TOML files to JSON
convert_toml_to_json(toml_file_list)

