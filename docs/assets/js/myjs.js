document.addEventListener("DOMContentLoaded", function () {
	// Fetch the JSON file
	fetch("json/combined_cards.json")
		.then((response) => response.json())
		.then((data) => {
			const cardGrid = document.getElementById("card-grid");
			console.log(cardGrid, " cardGrad");

			// Function to truncate text to a specified length
			const truncateText = (text, maxLength) =>
				text.length > maxLength ? text.slice(0, maxLength) + "..." : text;

			// Iterate over each freelancer object in the data array
			data.forEach((freelancer) => {
				console.log("freelancer : ", freelancer.data);
				// Create a card element for each freelancer
				const card = document.createElement("div");
				card.classList.add("card");

				// Determine the profile image (array or string)
				const profileImage = Array.isArray(freelancer.data.profile_image)
					? freelancer.data.profile_image[0]
					: freelancer.data.profile_image;

				// Truncate user description to a maximum length of 100 characters
				const truncatedDescription = truncateText(
					freelancer.data.user_description,
					100
				);

				// Construct the card's inner HTML
				card.innerHTML = `
                    <img align="right" src="${profileImage}" alt="${
					freelancer.data.name
				}" class="card-image">
                    <div class="card-title">${freelancer.data.name}</div>
                  
                   
                    <p class="card-description">${truncatedDescription}</p>
					  <p class = "tags"> <span class="tag_i_con">🏷️</span> ${freelancer.tags.join(", ")}</p>
                
                    <a href="freelancers/${
											freelancer.file_name
										}" ">Visit Profile</a>
                `;

				// Append the card to the card grid
				cardGrid.appendChild(card);
			});
		})
		.catch((error) => {
			console.error("Error fetching the JSON data:", error);
		});
});
