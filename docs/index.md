---
tags:
  - Website Pages
---

# Hur Freelancer Index

Being Free, A freer person. With Hur project freelancers are allowed to have their open public profiles shared with customers in an open and standard way that are viewable by human and processable by computer. Project is made possible by contributions of the Bitcoin Cash community. :warning: Project is still in development BETA stage.

<!--- Display Freelancer cards -->

## Freelancer Profiles

<style> * {margin: 0px;} </style>

<div class="card-grid" id="card-grid"></div>

<script src="assets/js/myjs.js"></script>

## How to Add a Freelancer

To add a freelancer to the project you:

1. Generate a profile using the [profile creation tool](https://hur-project.gitlab.io/hur-frontend-sveltekit/)
2. Store the file on any service that allows direct access to the file
3. Submit the URL using the profile creation tool

## How to Search for Freelancer or Service

You can use the search bar at the top of this website, or you can use the Tags page. I'm working on some better search tools with filtering.

## Create Your Own Static Website

You might use the single page component to have your own static site while also benefiting from the standard and easiness of Hur project. Check instructions at: https://gitlab.com/uak/mkdocs-hur-single-user



## Project Support and Discussion

* [Telegram channel](https://t.me/hur_project)
* Matrix - Coming soon d.v.
* SimpleX - Doesn't require creating an account - Coming soon d.v.


## Project Concept

The project utilize the TOML file format with Markdown format. the `convert_toml2md.py` scripts create a nice Markdown file from user profile that is in TOML format. Makrdown files then are used by a static site generator (Mkdocs Material) to create a nice web pages.

Hur is using Git version control for storing files as it offers transparency and helps with easier and verifiable distribution of content.

Hur is using Gitlab because it's an open source software that allows us or you to self host the project.

## Project Components 

Hur project has a set of tools:

* [Profile creation tool](https://hur-project.gitlab.io/hur-frontend-sveltekit/) to create profiles using the TOML format
* [Hur files - staging](https://gitlab.com/hur-project/hur-files-staging) stores users submissions before accepting in main repository
* [Hur files](https://gitlab.com/hur-project/hur) main repository for plain freelancers files
* [Hur Index](https://gitlab.com/hur-project/hur-website-index-mkdocs) Shows freelancers profiles in a beautiful static website
* [Hur single user](https://gitlab.com/uak/mkdocs-hur-single-user) Your own single user website hosted on Gitlab or Github
* [Hur backend](https://gitlab.com/hur-project/hur-backend) Handles submissions and database query for bot through API
* Hur bot - Should allow accessing freelancers data using Telegram bot (not published)


## License

Licensed under AGPL-V3 for entites that restricts knowledge sharing by licenses and patents.
